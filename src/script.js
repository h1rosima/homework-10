//1
const btn = document.createElement("a");
btn.textContent = 'Learn More';
btn.setAttribute('href', '#');

const footer = document.createElement('footer');
const paragraph = document.createElement("p");

document.body.append(footer);
footer.prepend(paragraph);
paragraph.after(btn);

//2
const selectMenu = document.createElement("select");
selectMenu.id = 'rating';

const main = document.createElement('main');
const features = document.createElement('section');
features.classList.add('Features');


document.body.prepend(main);
main.prepend(features);
features.before(selectMenu);

const options = [
    { value: "4", text: "4 Star" },
    { value: "3", text: "3 Star" },
    { value: "2", text: "2 Star" },
    { value: "1", text: "1 Star" }
];
options.forEach(rating => {
    const newOption = document.createElement("option");
    newOption.value = rating.value;
    newOption.textContent = rating.text;
    selectMenu.append(newOption);
});
